<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Responsive Admin Dashboard Template">
    <meta name="keywords" content="admin,dashboard">
    <meta name="author" content="stacks">

    <!-- Title -->
    <title>Dashboard</title>

    <!-- Styles -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;300;400;500;600;700;800&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp" rel="stylesheet">
    <link href="/adminAssets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/adminAssets/plugins/perfectscroll/perfect-scrollbar.css" rel="stylesheet">
    <link href="/adminAssets/plugins/pace/pace.css" rel="stylesheet">


    <!-- Theme Styles -->
    <link href="/adminAssets/css/main.min.css" rel="stylesheet">
    <link href="/adminAssets/css/custom.css" rel="stylesheet">

    <link rel="icon" type="image/png" sizes="32x32" href="/adminAssets/images/neptune.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="/adminAssets/images/neptune.png" />

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="app align-content-stretch d-flex flex-wrap">
    <div class="app-sidebar">
        <div class="logo">
            <a href="#" class="logo-icon"><span class="logo-text">Food Market</span></a>
            <div class="sidebar-user-switcher user-activity-online">
                <a href="#">
                    <img src="">
                    <span class="user-info-text">
                            {{auth()->user()->name}}
                        </span>
                </a>
            </div>
        </div>
        <div class="app-menu">
            <ul class="accordion-menu">
                <li class="sidebar-title">
                    Food Market
                </li>
                <li class="active-page">
                    <a href="#" class="active"><i class="material-icons-two-tone">dashboard</i>Dashboard</a>
                </li>
                @role('administrator')
                <li>
                    <a href=""><i class="material-icons-two-tone">star</i>Restaurant<i class="material-icons has-sub-menu">keyboard_arrow_right</i></a>
                    <ul class="sub-menu">
                        <li>
                            <a href="#">Liste</a>
                        </li>
                        <li>
                            <a href="#">Creer</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href=""><i class="material-icons-two-tone">star</i>Utilisateurs<i class="material-icons has-sub-menu">keyboard_arrow_right</i></a>
                    <ul class="sub-menu">
                        <li>
                            <a href="#">Liste</a>
                        </li>
                        <li>
                            <a href="#">Creer</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href=""><i class="material-icons-two-tone">star</i>Fournisseurs<i class="material-icons has-sub-menu">keyboard_arrow_right</i></a>
                    <ul class="sub-menu">
                        <li>
                            <a href="#">Liste</a>
                        </li>
                        <li>
                            <a href="#">Creer</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href=""><i class="material-icons-two-tone">star</i>Commandes<i class="material-icons has-sub-menu">keyboard_arrow_right</i></a>
                    <ul class="sub-menu">
                        <li>
                            <a href="#">Toutes les commandes par fournisseurs</a>
                        </li>
                        <li>
                            <a href="#">Commande validés</a>
                        </li>
                    </ul>
                </li>
                @endrole
                @role('fournisseur')
                <li>
                    <a href=""><i class="material-icons-two-tone">star</i>Articles<i class="material-icons has-sub-menu">keyboard_arrow_right</i></a>
                    <ul class="sub-menu">
                        <li>
                            <a href="#">Liste</a>
                        </li>
                        <li>
                            <a href="{{route('articles.create')}}">Creer</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href=""><i class="material-icons-two-tone">star</i>Commandes<i class="material-icons has-sub-menu">keyboard_arrow_right</i></a>
                    <ul class="sub-menu">
                        <li>
                            <a href="#">Commandes en attente</a>
                        </li>
                        <li>
                            <a href="#">Commandes en cours de livraison</a>
                        </li>
                        <li>
                            <a href="#">Commandes en livrées</a>
                        </li>
                    </ul>
                </li>
                @endrole
            </ul>
        </div>
    </div>
    <div class="app-container">
        <div class="search">
            <form>
                <input class="form-control" type="text" placeholder="Type here..." aria-label="Search">
            </form>
            <a href="#" class="toggle-search"><i class="material-icons">close</i></a>
        </div>
        <div class="app-header">
            <nav class="navbar navbar-light navbar-expand-lg">
                <div class="container-fluid">
                    <div class="navbar-nav" id="navbarNav">
                        <ul class="navbar-nav">
                            <li class="nav-item">
                                <a class="nav-link hide-sidebar-toggle-button" href="#"><i class="material-icons">first_page</i></a>
                            </li>
                        </ul>
                    </div>
                    <div class="d-flex">
                        <ul class="navbar-nav">

                            <li class="nav-item">
                                <a class="nav-link toggle-search" href="#"><i class="material-icons">search</i></a>
                            </li>
                            <li class="nav-item hidden-on-mobile">
                                <a class="nav-link" href="{{ route('logout') }}">Se deconnecter</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
        <div class="app-content">
            <div class="content-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="page-description">
                                <h1>Tableau de bord
                                    @role('administrator')
                                    Administrateur
                                    @endrole
                                    @role('fournisseur')
                                    Fournisseur
                                    @endrole
                                </h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Javascripts -->
<script src="/adminAssets/plugins/jquery/jquery-3.5.1.min.js"></script>
<script src="/adminAssets/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="/adminAssets/plugins/perfectscroll/perfect-scrollbar.min.js"></script>
<script src="/adminAssets/plugins/pace/pace.min.js"></script>
<script src="/adminAssets/plugins/apexcharts/apexcharts.min.js"></script>
<script src="/adminAssets/js/main.min.js"></script>
<script src="/adminAssets/js/custom.js"></script>
<script src="/adminAssets/js/pages/dashboard.js"></script>
</body>
</html>

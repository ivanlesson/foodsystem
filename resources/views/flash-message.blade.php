@if($message=Session::get('success'))
<div class="col-md-11 col-11">
	<div class="media align-items-center bg-success mb-20">
	{{$message}}
	</div>
</div>
@endif

@if($message=Session::get('modif'))
<div class="col-md-11 col-11">
	<div class="media align-items-center bg-warning mb-20">
		{{$message}}
	</div>
</div>
@endif

@if($message=Session::get('error'))
    <div class="col-md-11 col-11">
        <div class="media align-items-center bg-danger mb-20">
            {{$message}}
        </div>
    </div>
@endif

@if($message=Session::get('info'))
<div class="col-md-11 col-11">
	<div class="media align-items-center bg-info mb-20">
	{{$message}}
	</div>
</div>
@endif

<?php

use App\Http\Controllers\ArticleController;
use App\Http\Controllers\IndexController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [IndexController::class,'index'])->name('index');


Route::resource('produit',\App\Http\Controllers\ProduitController::class);

Route::get('/tendances', function () {
    return view('trending');
})->name('trending');

Route::get('/produit', function () {
    return view('product');
})->name('product');

Route::resource('articles',ArticleController::class);

Route::get('/logout', function(){
    Auth::logout();
    return Redirect::to('login');
});

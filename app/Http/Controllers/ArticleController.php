<?php

namespace App\Http\Controllers;

use App\Models\Article;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Intervention\Image\Facades\Image;

class ArticleController extends Controller
{

    public function __construct()
    {
        $this->middleware(['role:fournisseur']);

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $articles = DB::table('articles')
            ->select('id', 'photo', 'designation', 'description', 'prix', 'type')
            ->where('fournisseur_id', '=', auth()->user()->id)->get()->toArray(); // On recupere uniquement les articles du fournisseur
        return view('fournisseur.articles.index', ['articles' => $articles]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fournisseur.articles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        dd($request);
        $typeArticle = ['Frites', 'Pizza', 'Burger', 'Steak']; // Ceci est un tableau statique mais sera dynamisé apres étude complète du projet avec le client
        $validated = $request->validate([
            'photo' => 'required|image|mimes:jpg,jpeg,png,gif,svg|max:2048',
            'designation' => 'required|min:3',
            'description' => 'required|min:5',
            'prix' => 'required|numeric|min:3',
            'type' => 'required', Rule::in($typeArticle),
        ]);

        $image = $request->file('photo');
        $input['file'] = time() . '.' . $image->getClientOriginalExtension();

        $destinationPath = public_path('/fournisseur/articles');

        $destinationPath = public_path(DIRECTORY_SEPARATOR . "fournisseur" . DIRECTORY_SEPARATOR . "articles");

        $imgFile = Image::make($image->getRealPath());

        $imgFile->resize(360, 226)->save($destinationPath . DIRECTORY_SEPARATOR . $input['file']);

        $image->move($destinationPath, $input['file']);

        $fullUrl = $destinationPath . DIRECTORY_SEPARATOR . $input['file'];

        DB::beginTransaction(); // On demarre une transaction manuellement
        try {
            $article = new Article([
                'photo' => $fullUrl,
                'designation' => $request->designation,
                'description' => $request->description,
                'prix' => (int)$request->prix,
                'type' => $request->type,
                'fournisseur_id' => auth()->user()->id
            ]);
            $article->save;
        } catch (\Exception $e) {
            DB::rollBack(); // On annule la transaction
            Storage::delete($fullUrl); // Suppression de l'image precedement sotcké
            return back()->withErrors("Echec de l'opération");
        }
        DB::commit(); // On commit les informations dans la base de donées
        return redirect()->back()->with('success', 'Opération effectué avec success');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Article $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Article $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Models\Article $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Article $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        try {
            $article->delete();//On supprimer l'article apres confirmation
            unlink(public_path($article->photo));//Puis on supprime l'image sotcker
            return back()->with('success', 'Operation reussi');
        } catch (\Exception $e) {
            return back()->withErrors("Echec de l'opération");
        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * @return array
     * Fonction permettant de recuperer tous les articles
     */
    public function getAllArticleWithFournisseur(){
        return DB::table('articles')
            ->leftJoin('fournisseurs','articles.fournisseur_id','=','fournisseurs.id')->get()->toArray();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if(is_null(\auth()->user())){
            // Redirige le visieteur la page principal(ne necessite pas de connexion a l'application)
            $articles=$this->getAllArticleWithFournisseur();
            return view('index',['articles'=>$articles]);
        }
        if (Auth::user()->hasRole('administrator') || Auth::user()->hasRole('fournisseur')){
            return view('admin.index');
        }else{
            // Tout les roles autres administreur et formunisseur sont redirigé vers index avec le traitement lié a leur compte
            $articles=$articles=$this->getAllArticleWithFournisseur();
            return view('index',['articles'=>$articles]);
        }

    }

}
